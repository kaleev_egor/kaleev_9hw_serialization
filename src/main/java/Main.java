import list.CustomObject;
import proxy.ObjectHandler;
import services.IService;
import services.ObjectService;
import services.Service;
//import proxy.CashProxy;
import proxy.CashProxy;
import proxy.ListHandler;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        String path = "src/main/resources/cashFiles/";
        Service service = new Service();
        ListHandler listHandler = new ListHandler(service, path);
        CashProxy cashProxy = new CashProxy(listHandler);

        IService service1 = (IService) cashProxy.cach();

        System.out.println("list /////////////////////////////////////////////////////////////////////////////////////");
        run(service1);


        ObjectService objectService = new ObjectService();
        ObjectHandler objectHandler = new ObjectHandler(objectService, path);
        CashProxy cashProxy2 = new CashProxy(objectHandler);

        IService service2 = (IService) cashProxy2.cach();

        System.out.println("object /////////////////////////////////////////////////////////////////////////////////////");
        run2(service2);
    }

    private static void run(IService service) {
        List r1 = (List) service.doWork("work1", 10);
        System.out.println(r1);
        List r3 = (List) service.doWork("work1", 10);
        System.out.println(r3);
    }

    private static void run2(IService service) {
        CustomObject r1 = (CustomObject) service.doWork("work2", 10);
        System.out.println(r1.toString());
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            //ignore
        }
        r1 = (CustomObject) service.doWork("work2", 10);
        System.out.println(r1.toString());
    }
}
