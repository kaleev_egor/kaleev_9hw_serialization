package proxy;

import services.IService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class CashProxy {

    IService otherService;

    public CashProxy(InvocationHandler handler) {
        otherService = (IService) Proxy.newProxyInstance(IService.class.getClassLoader(),
                new Class[]{IService.class},
                handler);
    }

    public Object cach() {
        System.out.println("создан прокси");
        return otherService;
    }
}
