package proxy;

import static serialization.Serialization.*;

import annotations.Cachable;
import services.IService;
import list.ListServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

public class ListHandler implements InvocationHandler {

    IService service;

    String path;

    public ListHandler(IService service, String path) {
        this.service = service;
        this.path = path;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        List list = null;

        if (method.isAnnotationPresent(Cachable.class)) {
            Cachable cachable = method.getAnnotation(Cachable.class);
            boolean isZip = cachable.zip();
            String prefix = cachable.filePrefix();
            System.out.println(isZip + prefix);

            if (isZip) {
                args[0] += ".zip";
            }

            if (containsSerialF(path, prefix + args[1] + "_" + args[0])) {
                list = deserializeList(path + prefix + args[1] + "_" + args[0], isZip);
                System.out.println("произошла десериализация");
            } else {
                ListServiceImpl listServiceImpl = (ListServiceImpl) method.invoke(service, args);
                serializeList(listServiceImpl, path + prefix + listServiceImpl.getList().size() + "_" + listServiceImpl.getName(), isZip);
                System.out.println("запись нового файла");
                list = listServiceImpl.getList();
            }
        }
        return list;
    }
}
