package proxy;

import annotations.Cachable;
import list.CustomObject;
import services.IService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import static serialization.Serialization.*;

public class ObjectHandler implements InvocationHandler {

    IService service;

    String path;

    public ObjectHandler(IService service, String path) {
        this.service = service;
        this.path = path;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object obj = null;

        if (method.isAnnotationPresent(Cachable.class)) {
            Cachable cachable = method.getAnnotation(Cachable.class);
            boolean isZip = cachable.zip();
            String prefix = cachable.filePrefix();
            System.out.println(isZip + prefix);

            if (isZip) {
                args[0] += ".zip";
            }

            if (containsSerialF(path, prefix + args[0])) {
                obj = deerializeObject(path + prefix + args[0], isZip);
                System.out.println("произошла десериализация");
            } else {
                CustomObject customObject = (CustomObject) method.invoke(service, args);
                serializeObject(customObject, path + prefix + customObject.getName(), isZip);
                System.out.println("запись нового файла");
                obj = customObject;
            }
        }

        return obj;
    }
}
