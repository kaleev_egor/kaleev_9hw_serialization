package list;

import java.io.Serializable;
import java.util.Date;

public class CustomObject implements Serializable {
    private String name;
    private Date creationDate;
    private Date serializationDate = null;
    private transient Date deserializationDate = null;

    public CustomObject(String name) {
        this.name = name;
        this.creationDate = new Date();
    }

    public String getName() {
        return name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getSerializationDate() {
        return serializationDate;
    }

    public void setSerializationDate(Date serializationTime) {
        this.serializationDate = serializationTime;
    }

    public Date getDeserializationDate() {
        return deserializationDate;
    }

    public void setDeserializationTime(Date deserializationTime) {
        this.deserializationDate = deserializationTime;
    }

    @Override
    public String toString() {
        return "CustomObject{" +
                "name='" + name + '\'' +
                ", creationTime=" + creationDate +
                ", serializationTime=" + serializationDate +
                ", deserializationTime=" + deserializationDate +
                '}';
    }
}
