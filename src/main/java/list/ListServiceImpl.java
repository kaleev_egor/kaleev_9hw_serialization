package list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class ListServiceImpl implements Serializable {

    private List list;
    private String name;

    public List getList() {
        return list;
    }

    public String getName() {
        return name;
    }

    public ListServiceImpl(int length, String name) {
        list = new ArrayList(length);
        this.name = name;
        fillAList(length);
    }

    private void fillAList(int length) {
        Random rnd = new Random();
        for (int i = 0; i < length; i++) {
            list.add(rnd.nextInt());
        }
    }

    @Override
    public String toString() {
        return "ListServiceImpl{" +
                "list=" + list +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListServiceImpl that = (ListServiceImpl) o;
        return Objects.equals(list, that.list) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list, name);
    }
}
