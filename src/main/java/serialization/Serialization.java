package serialization;

import list.CustomObject;
import list.ListServiceImpl;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Serialization {

    public static void serializeList(ListServiceImpl listServiceImpl, String path, boolean isZip) {
        try (FileOutputStream fos = new FileOutputStream(path)) {

            serialize(listServiceImpl.getList(), isZip, fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List deserializeList(String path, boolean isZip) {

        List list = null;

        try (FileInputStream fis = new FileInputStream(path)) {

            ObjectInputStream ois = getObjectInputStream(path, isZip, fis);
            ;

            list = (List) ois.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void serializeObject(CustomObject customObject, String path, boolean isZip) {

        try (FileOutputStream fos = new FileOutputStream(path)) {
            customObject.setSerializationDate(new Date());

            serialize(customObject, isZip, fos);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void serialize(Object object, boolean isZip, FileOutputStream fos) throws IOException {
        ObjectOutputStream oos;
        if (isZip) {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(b);
            oos.writeObject(object);
            byte[] buffer = b.toByteArray();

            BufferedOutputStream bos = new BufferedOutputStream(fos);

            try (ZipOutputStream zos = new ZipOutputStream(bos)) {
                zos.putNextEntry(new ZipEntry("file"));
                zos.write(buffer, 0, buffer.length);
                zos.closeEntry();
            }
        } else {
            oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
        }
    }

    public static CustomObject deerializeObject(String path, boolean isZip) {

        CustomObject customObject = null;

        try (FileInputStream fis = new FileInputStream(path)) {

            ObjectInputStream ois = getObjectInputStream(path, isZip, fis);
            customObject = (CustomObject) ois.readObject();
            customObject.setDeserializationTime(new Date());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return customObject;
    }

    private static ObjectInputStream getObjectInputStream(String path, boolean isZip, FileInputStream fis) throws IOException {
        ObjectInputStream ois;
        if (isZip) {
            ZipFile zipFile = new ZipFile(path);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            ZipEntry entry = entries.nextElement();

            InputStream stream = zipFile.getInputStream(entry);

            ois = new ObjectInputStream(stream);
        } else {
            ois = new ObjectInputStream(fis);
        }
        return ois;
    }

    public static boolean containsSerialF(String path, String fileName) {
        File dir = new File(path);
        List<String> lst = new ArrayList<>();
        for (File file : dir.listFiles()) {
            if (file.isFile())
                lst.add(file.getName());
        }
        if (lst.contains(fileName)) {
            return true;
        }
        return false;
    }
}
