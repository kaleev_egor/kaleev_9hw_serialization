package services;

import list.ListServiceImpl;

public class Service implements IService {
    @Override
    public Object doWork(String name, int length) {
        ListServiceImpl listService = new ListServiceImpl(length, name);
        return listService;
    }
}
