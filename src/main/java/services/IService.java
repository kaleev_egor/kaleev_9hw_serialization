package services;

import annotations.Cachable;

public interface IService {
    @Cachable(filePrefix = "M", zip = true)
    Object doWork(String name, int length);
}
