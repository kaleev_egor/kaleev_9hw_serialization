package services;

import list.CustomObject;

public class ObjectService implements IService {
    @Override
    public Object doWork(String name, int length) {
        CustomObject customObject = new CustomObject(name);
        return customObject;
    }
}
